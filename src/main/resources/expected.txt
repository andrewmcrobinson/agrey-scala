Product Id,Description,Count,Unit Price,Vat,Line Total
1,6 pack-tumblers,2,R80.00,R11.20,R182.40
2,10 pack-swizzle sticks,4,R5.00,R0.70,R22.80
3,Bag of ice,5,R10,R1.40,R57
4,2l Coke,2,R20,R2.80,R45.60
5,2l Sprite,2,R18.50,R2.59,R42.18
Total,,,,R18.69,R349.98
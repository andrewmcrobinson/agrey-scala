package com.andrew

class Item(val productId: String, val description: String, unitPriceAsString: String) {
  
  //so we should have getters only for productId and description, and no access to unitPriceAsString

  // some class fields
  private val vatRate = BigDecimal("0.14");
  
  val unitPrice = BigDecimal(unitPriceAsString)
  val vat = unitPrice * vatRate
  val unitPricePlusVat = unitPrice + vat
  
  //we should now also have getters for unitPrice, vat and unitPricePlusVat

  //TODO - we want only a getter, no setter, how?
  var count=1
  
  def incrementCount() {
	  count += 1
  }
  
}


package com.andrew

import scala.io.Source
import scala.collection.mutable.ListBuffer
import collection.SortedMap

/**
 * Assumptions: 
 * 
 * 1. The supplied expected output shouldn't (can't) be 100% emulated since its currency 
 * formatting is inconsistent. (Sometimes the format is Rands and Cents, other times just Rands)
 *
 * Below is the output from the unit test written to drive my development
 *
 * --------------------------------------------------------------
 * line 3: actualLine not equal to expectedLine
 * line 3: actualLine:'3,Bag of ice,5,R10.00,R1.40,R57.00'
 * line 3: expectedLine:'3,Bag of ice,5,R10,R1.40,R57'
 *
 * line 4: actualLine not equal to expectedLine
 * line 4: actualLine:'4,2l Coke,2,R20.00,R2.80,R45.60'
 * line 4: expectedLine:'4,2l Coke,2,R20,R2.80,R45.60'
 * --------------------------------------------------------------
 * However, on line 2, the expected unit price is R5.00, not R5, so there seems to me to be no
 * consistent rule for this that I can implement.
 * 
 * 2. Although use of an external CSV-parsing library is a better idea for a production system,
 * I've deemed this just a distraction for the purposes of this exercise.
 * 
 * 3. For error handling, I've just coded up some simple messages to stdout. 
 *    Without more information about the production environment in which this would run it doesn't 
 *    feel like I can do much more.
 *    
 * @author andrew
 *
 */
object Parser extends App {

	val inputFilepath = "src/main/resources/invoiceItems2.txt";
	val p = new Parser
	
	try {
		val output = p.buildOutputFromCsv(inputFilepath)    
		output.foreach(line => println(line))
	} catch {
	  case e: Exception => println("ERROR: exception caught and processing aborted: " + e);
	}
	

 
}

class Parser {
  
  def buildOutputFromCsv(filename: String): ListBuffer[String] = {

	  //Read the file and build up our data structures
      val bufferedSource = Source.fromFile(filename)

      //Items are keyed against product_id
      var uniqueItemDetails = SortedMap[String, Item]()

      //Now iterate through the file lines and build up our data structures
      for (line <- bufferedSource.getLines.zipWithIndex.filter(_._2 != 0).map(_._1)) {
    	  val cols = line.split(",").map(_.trim)

    	  if (uniqueItemDetails.contains(cols(0))) {
    	    uniqueItemDetails(cols(0)).incrementCount
    	  } else {
    	    
    		  try {
    			val item = new Item(cols(0),cols(2),cols(1))
    			uniqueItemDetails += (cols(0) -> item)
    		  } catch {
    		    case e: Exception => {
    		      println(s"ERROR: exception caught instantiating item with values: '${cols(0)}','${cols(2)}','${cols(1)}'");
    		      throw(e)
    		    }
    		  }

    	  }
        
      }
      
      bufferedSource.close
      
//		Desired output:      
//      Product Id,Description,Count,Unit Price,Vat,Line Total
//      1,6 pack-tumblers,2,R80.00,R11.20,R182.40

      var outputLines = ListBuffer[String]()
      outputLines.append("Product Id,Description,Count,Unit Price,Vat,Line Total")

      //Set up running totals
      var totalVat=BigDecimal("0")
      var totalLineTotal=BigDecimal("0")
      
      //Now iterate over uniqueItemDetails to build our desired output
      for ((productId,item) <- uniqueItemDetails) {
        val lineTotal = item.count * item.unitPricePlusVat
        outputLines += s"${productId},${item.description},${item.count},R${item.unitPrice.formatted("%.2f")},R${item.vat.formatted("%.2f")},R${lineTotal.formatted("%.2f")}"

        totalVat = totalVat + item.vat
        totalLineTotal = totalLineTotal + lineTotal
        
      }

  	  outputLines += s"Total,,,,R${totalVat.formatted("%.2f")},R${totalLineTotal.formatted("%.2f")}"
      
      return outputLines
      
  } 
  
}
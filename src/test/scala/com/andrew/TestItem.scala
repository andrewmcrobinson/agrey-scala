package com.andrew

import scala.io.Source
import org.scalatest._

class TestItem extends FlatSpec with Matchers{

  it should "calculate correct vat and unitPricePlusVat amounts when instantiated using only productId, description and unitPriceAsString" in {
    
    	val item = new Item("1", "Bag of ice", "100");
	    	
    	val expectedUnitPrice = BigDecimal("100.00");  	
    	val expectedVat = BigDecimal("14.00");  	
		val expectedUnitPricePlusVat = BigDecimal("114.00");
		
		item.productId should be ("1")
		item.description should be ("Bag of ice")
		item.unitPrice should be (expectedUnitPrice)
		item.vat should be (expectedVat)
		item.unitPricePlusVat should be (expectedUnitPricePlusVat)

		item.count should be (1)
		item.incrementCount
		item.count should be (2)
		
		//TODO - this should not be possible
		item.count=99
		item.count should be (99)
		
  }

  it should "Should handle cents too, displaying only to two decimal places" in {
	  
	  val item = new Item("1", "Bag of ice", "25");
	  
	  val expectedUnitPrice = BigDecimal("25.00");  	
	  val expectedVat = BigDecimal("3.50");  	
	  val expectedUnitPricePlusVat = BigDecimal("28.50");
	  
	  item.productId should be ("1")
	  item.description should be ("Bag of ice")
	  item.unitPrice should be (expectedUnitPrice)
	  item.vat should be (expectedVat)
	  item.unitPricePlusVat should be (expectedUnitPricePlusVat)

	  item.count should be (1)
	  item.incrementCount
	  item.count should be (2)
	  
  }
  
}
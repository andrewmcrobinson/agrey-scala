package com.andrew

import scala.io.Source
import org.scalatest._

class TestParser extends FlatSpec with Matchers{

  it should "produce the desired output from the specified input" in {

    //First read in the expected output
    val expectedOutputFilepath = "src/main/resources/expected.txt";
    val bufferedSource = Source.fromFile(expectedOutputFilepath)
    var expectedOutputLines = Vector[String]()
  
    for (line <- bufferedSource.getLines) {
    	expectedOutputLines = expectedOutputLines :+ line
    }
    bufferedSource.close

    
    //Now get the actual output
    val inputFilepath = "src/main/resources/invoiceItems.txt";
	val p = new Parser
    val actualOutputLines = p.buildOutputFromCsv(inputFilepath)    
    
    //Now compare them
    if (actualOutputLines.size != expectedOutputLines.size){
//    	fail("actualOutputLines.size:"+actualOutputLines.size+" != expectedOutputLines.size:"+expectedOutputLines.size);
	}
    
	var i=0;

	for (actualLine <- actualOutputLines) {			
		
		val expectedLine = expectedOutputLines(i)
		
		if (!actualLine.equals(expectedLine)){
			println("line "+i+": actualLine not equal to expectedLine");
			println("line "+i+": actualLine:'"+actualLine+"'");
			println("line "+i+": expectedLine:'"+expectedLine+"'");
			println();
		}

		i=i+1
	}

	
  }

}
